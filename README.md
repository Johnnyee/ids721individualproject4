# IDS721 Individual Project 4: Rust AWS Lambda and Step Functions

## Requirements
The project involves setting up a data processing pipeline using Rust AWS Lambda functions and AWS Step Functions. The key components include:
- **Rust AWS Lambda function** that parses input text and calculates character frequencies.
- **AWS Step Functions** workflow that coordinates the execution of Lambda functions.

## Rust Lambda Functionality
The Lambda functions include:
- `Aggregate`: 
The provided Rust code defines an AWS Lambda function that aggregates car data by make and model. It receives a list of cars, calculates the total count and average price for each make and model, and outputs these statistics organized in a sorted format. This function runs asynchronously, leveraging Tokio for efficient event handling within AWS Lambda's environment.


- `Filter`:  It processes an input AggregateOutput, which contains a collection of CarStats mapped by car make and model. Each CarStats holds information about the total number of cars and their average price. The function, named filter, extracts entries where the average price exceeds $22,000 and places these into a new BTreeMap, ensuring the results are sorted. This filtered data is then wrapped in a FilterOutput structure containing the high_value_cars map. The function runs asynchronously within AWS Lambda's environment, utilizing Tokio for handling asynchronous tasks efficiently.


## Project Steps

### Create Rust Lambda Project
1. Create a new lambda project with `cargo lambda new <PROJECT_NAME>`.
2. Update `Cargo.toml` with the necessary dependencies.
3. Implement the functions in `main.rs`.

### Test Locally
To test the lambda functions locally:
```bash
cd calculate_frequency
cargo lambda watch
cargo lambda invoke --data-file ../input.json
```

### AWS Step Functions
1. Create a New AWS IAM User:
- Navigate to AWS IAM and create a new user. Attach the following policies: `iamfullaccess` and `lambdafullaccess`, along with any other necessary policies.
- In the Security Credentials section, generate a new access key for this user.
- Utilize this user's credentials for all your AWS functions.
2. Set Up Environment Variables:
- Create a `. env ` file and include your AWS access key ID, secret access key, and region in the following format:
```
AWS_ACCESS_KEY_ID=your_access_key_id
AWS_SECRET_ACCESS_KEY=your_secret_access_key
AWS_REGION=your_aws_region
```
3. Configure Git to Ignore Sensitive Files:
- Create a `.gitignore` file and ensure it includes entries to ignore the `target ` directory and the `.env` file:
```
/target
. env
```
4. Load Environment Variables:
- Load the environment variables into your terminal session by either exporting them directly or by using the `set - a ` command followed by `source . env`.
5. Deploy Your AWS Lambda Functions:
- Deploy each of your functions separately to AWS Lambda. After deployment, verify that each function is correctly listed in the AWS Lambda console.
6. Create and Configure AWS Step Functions:
- Go to AWS Step Functions and start the creation of a new state machine.
- Use the visual editor to drag the lambda invoke actions corresponding to each of your deployed functions. Arrange them in the sequence you have planned.
- In the code editor, you can view and save the configuration of your state machine into a JSON file:
```
{
  "Comment": "A workflow to calculate average  and filter high price.",
  "StartAt": "Aggregate",
  "States": {
    "Aggregate": {
      "Type": "Task",
      "Resource": "FUNCTION ARN",
      "Next": "Filter"
    },
    "Filter": {
      "Type": "Task",
      "Resource": "FUNCTION ARN",
      "End": true
    }
  }
}
```
7. Name and Test Your Step Function:
- Assign a name to your state machine and create it.
- Test the step function by starting an execution from the console and inputting your test case. A successful execution should complete without errors.


## Screenshot
![original image](https://cdn.mathpix.com/snip/images/5suIDMBs74f1F68rnCc59kNc7CZjzwMZefh79ickPsw.original.fullsize.png)

![original image](https://cdn.mathpix.com/snip/images/0ttkVKExiZNkyy_BcduKx5MGNbvIPl-9nr4n3VauzdE.original.fullsize.png)


![original image](https://cdn.mathpix.com/snip/images/jmvapnDw5PtJ_3FfgWsDYxgIrfJC4RCVxOfYy7ghmnk.original.fullsize.png)


![original image](https://cdn.mathpix.com/snip/images/8rxiVaTuz7OQ_Qh5Th9QQhCMh7QR301zrlhK_rv50BY.original.fullsize.png)

## Demo Video
![Deomo Video](https://gitlab.com/Johnnyee/ids721individualproject4/-/wikis/uploads/8165bea3c6362a0cd7c63b296ab562ab/IndividualP4.mp4)


