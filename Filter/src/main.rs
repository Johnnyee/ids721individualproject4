use lambda_runtime::{handler_fn, Context, Error};
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;

#[derive(Deserialize, Serialize, Debug)]
struct CarStats {
    total_count: usize,
    average_price: u32,
}

#[derive(Deserialize, Serialize, Debug)]
struct AggregateOutput {
    aggregates: BTreeMap<String, CarStats>,
}

#[derive(Deserialize, Serialize, Debug)]
struct FilterOutput {
    high_value_cars: BTreeMap<String, CarStats>,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = handler_fn(filter);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn filter(event: AggregateOutput, _ctx: Context) -> Result<FilterOutput, Error> {
    let mut high_value_cars = BTreeMap::new();

    for (key, stats) in event.aggregates {
        if stats.average_price > 22000 {
            high_value_cars.insert(key, stats);
        }
    }

    Ok(FilterOutput {
        high_value_cars,
    })
}
