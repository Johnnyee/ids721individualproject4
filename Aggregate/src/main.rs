use lambda_runtime::{handler_fn, Context, Error};
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, BTreeMap};

#[derive(Deserialize, Serialize, Debug)]
struct Car {
    make: String,
    model: String,
    year: u32,
    price: u32,
}

#[derive(Deserialize, Serialize, Debug)]
struct AggregateOutput {
    aggregates: BTreeMap<String, CarStats>,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
struct CarStats {
    total_count: usize,
    average_price: u32,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = handler_fn(aggregate);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn aggregate(event: Vec<Car>, _ctx: Context) -> Result<AggregateOutput, Error> {
    let mut summaries = HashMap::new();

    for car in event {
        let key = format!("{} {}", car.make, car.model);
        let entry = summaries.entry(key).or_insert_with(|| CarStats { total_count: 0, average_price: 0 });
        entry.total_count += 1;
        entry.average_price = ((entry.average_price * (entry.total_count - 1) as u32) + car.price) / entry.total_count as u32;
    }

    Ok(AggregateOutput {
        aggregates: summaries.into_iter().collect(),
    })
}